# CREATE
cucumber features/scenarios/user/createNewUser.feature
cucumber features/scenarios/user/loginUser.feature
cucumber features/scenarios/reports/campaign/createGraph.feature
cucumber features/scenarios/reports/events/createGraph.feature
# CONFIGURE
cucumber features/scenarios/events/configure.feature
cucumber features/scenarios/reports/campaign/configureGraph.feature
cucumber features/scenarios/reports/events/configureGraph.feature
# VIEW MORE DETAILS
cucumber features/scenarios/reports/events/viewGraph.feature
cucumber features/scenarios/reports/campaign/viewGraph.feature
# DELETE GRAPH
cucumber features/scenarios/reports/events/deleteGraph.feature
cucumber features/scenarios/reports/campaign/deleteGraph.feature

#RUN CREATE - CONFIGURE graph from Reportes page
cucumber --require features features/scenarios/reports/campaign/createGraph.feature features/scenarios/reports/campaign/configureGraph.feature features/scenarios/reports/campaign/viewGraph.feature features/scenarios/reports/campaign/deleteGraph.feature