require_relative '../classes/navigation.rb'
navigation = Navigation.new()

#BACKGROUND NAVIGATE TO REPORTES - EVENTOS PAGE
Given('I navigate to the login page') do
  visit '/'
end

When('I submit username and password') do
  fill_in 'usernameField' , :with => ENV['USER']
  fill_in 'passwordField' , :with => ENV['PSW']
  xpath_base = '/html/body/app-root/div/div/app-login/div/div/div[2]/form/div/form/div/div[4]/div/button'
  find(:xpath, xpath_base).click
end

When('I Navigate to Reportes - Eventos page') do
  navigation.navigateToReportesEventosPage
end

#FOR CREATE GRAPH
When('Press the bottom Agregar Grafica') do
  xpath_css = 'html body.ng-tns-0-0 app-root div div.header-padding app-event-reports div.col-lg-12 div.row div.col div.chart-add-button button.btn.btn-outline-primary.button-create'
  find(:css , xpath_css).click
end

#FOR CREATE GRAPH ON CAMPAING PAGE
When ('I Navigate to Reportes - Campaña page') do
  navigation.navigateToReportesCampañaPage
end

When('I select FarmaCorp enterprise - campaing DemoTest') do
  cssPath_Enterprise = '.campaign-items > div:nth-child(5) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)'
  cssPath_Capaing = '.subcategories-one > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)'
  find(:css,cssPath_Enterprise).click
  find(:css,cssPath_Capaing).click
end