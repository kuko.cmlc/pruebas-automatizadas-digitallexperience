Given('I enter my user and password') do
  fill_in 'usernameField', :with => ENV['USER']
  fill_in 'passwordField', :with => ENV['PSW']
end

When('I press the {string} button') do |string|
  xpath = 'body > app-root > div > div > app-login > div > div > div.col-6 > form > div > form > div > div.row.m-t-30 > div > button'
  find(:css, xpath).click
end

Then('I am in the {string} page') do |string|
  if(string=="home")
    expect(page).to have_content("Administración")
  else 
    expect(page).to have_content("Login")
    expect(page).to have_content("Fallo al iniciar sesión")
  end
end

Given('I enter a user no valid') do
  fill_in 'usernameField', :with => "CARLOS"
  fill_in 'passwordField', :with => "1234.2"
end
