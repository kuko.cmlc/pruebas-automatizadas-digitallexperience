require_relative '../../classes/editEvent.rb'

editEvent= EditEvent.new()

Given('I am on home page of event') do
  editEvent.validateIamOnPageEvents()
end

Given('I select an event') do
  editEvent.selectEvent
end

Given('I enter empty data') do
  editEvent.emptyTheFields
end

Given('I press the Editar button of event') do
  editEvent.pressButton
end

Then('I should see the message after edit {string}') do |string|
  expect(page).to have_content(string)
end

Given('I enter the required fields of the event') do
  editEvent.validData
end