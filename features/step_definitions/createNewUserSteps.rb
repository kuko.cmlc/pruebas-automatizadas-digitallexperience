Given('I am on the DigitalAll Experience homepage') do
  page.driver.browser.manage.window.maximize
  visit('/')
end

Given('I enter my user and password and I press the {string} button') do |string|
  fill_in 'usernameField', :with => ENV['USER']
  fill_in 'passwordField', :with => ENV['PSW']
  xpath = 'body > app-root > div > div > app-login > div > div > div.col-6 > form > div > form > div > div.row.m-t-30 > div > button'
  find(:css, xpath).click
end

Given('I click the {string} dropdown') do |string|
  fullXpath='/html/body/app-root/div/app-header/nav/div/div/a'
  find(:xpath, fullXpath).click
end

Given('select {string} option') do |string|
  cssPath='#dropdown-basic > a:nth-child(2)'
  find(:css, cssPath).click
end

Given('I press the Crear Usuario button') do
  cssSelector='body > app-root > div > div > app-users > div > div.row.user-add-filters > div.col-4 > div > button'
  find(:css, cssSelector).click
end

When('I enter the required fields') do
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(1) > input').set("Paola13")
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(2) > input').set("paola13@gmail.com")
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(3) > input').set("76567834")
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(4) > input').set("paoo13")
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(5) > input').set("123456.")
  find(:css, 'body > modal-container > div > div > app-create-user > div > form > div.modal-body > div:nth-child(6) > select > option:nth-child(2)').click
end

When('I press the confirmation button') do 
  find(:css,'body > modal-container > div > div > app-create-user > div > form > div.modal-footer > button.btn.btn-outline-primary.btn-sm.button-create').click
end

Then('the create user successfully message is displayed') do
  #raise "Error User could not be created, It is a Bug."
  sleep 0.5
  expect(page).to have_content('Usuario creado con éxito')
end

Then('the create user failure message is displayed') do
  expect(page).to have_content('Error al crear el usuario!')
end