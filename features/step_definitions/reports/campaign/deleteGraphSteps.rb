require_relative '../../../classes/deleteGraph.rb'
deleteGraph = DeleteGraph.new()

Given('I am on Reportes page') do
  page.has_css?('body > app-root > div > div > app-reports > div > ng-sidebar-container > ng-sidebar > aside')
  page.has_css?('body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div')
end

When('I click the button Borrar of a graph type {string}') do |string|
  page.execute_script "window.scrollBy(0,1000)"
  deleteGraph.deleteGraphCampaingPage(string)
end

Then('I should not see that graph\/chart in the list.') do
  deleteGraph.validateGraphDeleted
end