require_relative '../../../classes/configureGraph.rb'
configureGraph = ConfigureGraph.new()
nameGraph= "New Chart";
typeGraph = "New Type Chart";

Given('I have a chart type {string} with name {string} created') do |string, string2|
  @nameGraph = string2
  @typeGraph = string
  configureGraph.selectGraph(@nameGraph,@typeGraph)
end

When('I edit the chart with valid data') do
  page.execute_script "window.scrollBy(0,10000)"
  configureGraph.startConfigureGraph(@nameGraph,@typeGraph)
end

Then('I should see the garph {string} with the data that I put') do |string|
  page.execute_script "window.scrollBy(0,10000)"
  configureGraph.validateGraphConfigurated(@typeGraph)
end