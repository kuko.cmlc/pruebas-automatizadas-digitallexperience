Given('I am on Reportes page and the graph type Contador exist') do
  page.has_css?('body > app-root > div > div > app-reports > div > ng-sidebar-container > ng-sidebar > aside')
  page.has_css?('body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(3) > chart > div > div')
end

When('I click the button Ver Mas of a graph type Contador') do
  find(:css , 'body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(3) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(1) > i').click
end

Then('I should see the garph maximized.') do
  page.has_css?('body > modal-container > div > div > app-full-view > div > div.modal-header > div > h5')
  page.has_css?('body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > counter > div:nth-child(4) > div')
  page.has_css?('body > modal-container > div > div > app-full-view > div > div.modal-header > div > h4')
  page.has_css?('body > modal-container > div > div > app-full-view > div > div.modal-header > button')
end