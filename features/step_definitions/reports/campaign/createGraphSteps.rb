
totalCharts = 0
Given('I click the button Agregar Grafica') do
  @totalCharts = all('chart').size 
  cssPath_button_create_graph = '.btn'
  find(:css , cssPath_button_create_graph).click
end

When('I create a new graph with the same name of other') do
  fill_in('title', :with => 'Nuevo Chart')
  find(:css, "select.ng-valid > option:nth-child(5)").click  
  find(:css, "option.ng-star-inserted:nth-child(3)").click  
  find(:css, "button.btn:nth-child(2)").click
end

When('I create a new graph with no data') do
  fill_in('title', :with => '')
  find(:css, "button.btn:nth-child(2)").click
  find(:css, ".close").click
end

Then('it should not be created') do
  sleep 0.5
  number_charts = all('svg').size 
  if number_charts > @totalCharts
    raise "The chart was created, It is an error."
  end
end

Then('I create a new graph type Contador') do
  fill_in('title', :with => 'Grafico Contador')
  find(:css, "select.ng-valid > option:nth-child(7)").click  
  find(:css, "option.ng-star-inserted:nth-child(2)").click  
  find(:css, "button.btn:nth-child(2)").click
end

Then('it should be created with empty data') do
  sleep 0.5
  number_charts = all('svg').size 
  find(:css, "div.col-6:nth-child(#{number_charts}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)").click  
  find(:css, "div.col-6:nth-child(#{number_charts}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1)").click  
end