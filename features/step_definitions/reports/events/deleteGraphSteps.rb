require_relative '../../../classes/deleteGraph.rb'
deleteGraph = DeleteGraph.new()

Given('I have a graph {string} created before on the Reportes - Eventos page') do |string|
  page.execute_script "window.scrollBy(0,10000)"
  deleteGraph.validateGraphExist(string)
end

When('I click the trash icon from the graph {string}') do |string|
  deleteGraph.deleteGraphFromEventPage(string)
end

Then('I should see all the graphs except the graph that was removed.') do
  deleteGraph.validateGraphDeletedFromEventPage
end