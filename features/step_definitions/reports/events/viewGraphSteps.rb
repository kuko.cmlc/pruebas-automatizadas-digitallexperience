require_relative '../../../classes/viewGraph.rb'

viewGraph = ViewGraph.new()
counterCharts = 0

Given('I am on the Reportes - Eventos page') do
  page.execute_script "window.scrollBy(0,10000)"
  viewGraph.validateIAmOnEventosPage
end

When('I count all the old charts created') do
  $counterCharts = all('chart').size 
end

Then('I should have more than {int} charts.') do |int|
  if $counterCharts<int
    raise "There is an error with counting the charts"
  end
end

When('I click the eye icon from the graph {string}') do |string|
  viewGraph.clickEyeIcon(string)
end

Then('I should see the graph {string} on the entire page.') do |string|
  viewGraph.validateGraphDisplayed(string)
end