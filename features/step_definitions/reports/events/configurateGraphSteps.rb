require_relative '../../../classes/configureGraph.rb'
require_relative '../../../classes/viewGraph.rb'

viewGraph = ViewGraph.new()
configureGraph = ConfigureGraph.new()

When('I select a graph with name {string} and type {string}') do |string, string2|
  configureGraph.startConfigureGraph(string,string2)
  page.execute_script "window.scrollBy(0,10000)"
end

Then('I should see the message {string}') do |string|
  expect(page).to have_content("Configuración actualizada con éxito!")
end

Then('see the new graph with name {string}.') do |string|
  expect(page).to have_content(string)
end