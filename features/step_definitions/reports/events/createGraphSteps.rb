require_relative '../../../classes/createGraph.rb'

createGraph = CreateGraph.new()
Given('I am on the form for create a new chart') do
  createGraph.validateModalDisplayed
end

When('I create a new {string} type graph with name {string}') do |string, string2|
  createGraph.createGraph(string2,string)
end

Then('I should see the new graph with name {string}.') do |string|  
  createGraph.checkGraphCreated(string)
end