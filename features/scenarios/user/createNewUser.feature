Feature: Create New User
  As a DigitalAll Experience user
  I want to create a new user
  so I will have more users who use the coin application

Background:
  Given I am on the DigitalAll Experience homepage
  And I enter my user and password and I press the "Iniciar Sesión" button
  And I click the "Administración" dropdown 
  And select "Usuarios" option
  
Scenario: Create user with valid data
  And I press the Crear Usuario button
  When I enter the required fields
  And I press the confirmation button
  Then the create user successfully message is displayed

Scenario: Create user with invalid data
  And I press the Crear Usuario button
  When I enter the required fields
  And I press the confirmation button
  Then the create user failure message is displayed

