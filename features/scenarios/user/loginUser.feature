Feature: Login DigitalAll Experience
  As a DigitalAll Experience user
  I want to login in the system
  so I want use the application
Background:
  Given I am on the DigitalAll Experience homepage

@loginSuccessful
Scenario: Login with valid user
  And I enter my user and password 
  When I press the "Iniciar Sesión" button
  Then I am in the "home" page

@loginFail
Scenario: Login with invalid user
  And I enter a user no valid
  When I press the "Iniciar Sesión" button
  Then I am in the "login" page 