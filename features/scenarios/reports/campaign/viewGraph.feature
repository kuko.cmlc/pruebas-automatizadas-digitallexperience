Feature: See more details from a graph/chart
  As administrador of FarmaCorp enterprise
  I want to see more details of a specific chart 
  so I can see the graph maximized to create more detailed reports

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Campaña page
 And I select FarmaCorp enterprise - campaing DemoTest

@maximize
Scenario: View more details of a chart type Contador
  Given I am on Reportes page and the graph type Contador exist 
  When I click the button Ver Mas of a graph type Contador
  Then I should see the garph maximized.