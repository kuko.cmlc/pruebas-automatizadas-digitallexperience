Feature: Create a new graph to configura
  As administrador of FarmaCorp enterprise
  I want to see all the graphs created
  so I can canfigurate that graph for generate new reports

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Campaña page
 And I select FarmaCorp enterprise - campaing DemoTest

@maximize
Scenario: Create a graph with empty data
  Given I click the button Agregar Grafica
  When I create a new graph with no data 
  Then it should not be created

@maximize
Scenario: Create a graph with the same name is not possible
  Given I click the button Agregar Grafica
  When I create a new graph with the same name of other
  Then it should not be created

@maximize
Scenario: Create a graph type "Contador"
  Given I click the button Agregar Grafica
  When I create a new graph type Contador
  Then it should be created with empty data
