Feature: Configure a new graph for generate reports 
  As administrador of FarmaCorp enterprise
  I want to see all the graphs created
  so I can compare old and new charts to get metrics and take decisions

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Campaña page
 And I select FarmaCorp enterprise - campaing DemoTest

@maximize
Scenario: Edit a graph type Contador and Barra Horizontal     
  Given I have a chart type "<tipo_grafico>" with name "<nombre_grafica>" created
  When I edit the chart with valid data
  Then I should see the garph "<nombre_grafica>" with the data that I put

Examples:
    | nombre_grafica           | tipo_grafico     |
    | Grafica Contador         | Contador         |
    | Grafica Barra Horizontal | Barra Horizontal |
