Feature: Delete a graph created
  As administrador of FarmaCorp enterprise
  I want to delete a graph created
  so I can remove all the graph duplicated or obsolete

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Campaña page
 And I select FarmaCorp enterprise - campaing DemoTest

@maximize
Scenario: Delete an old or obsolete graph
  Given I am on Reportes page 
  When I click the button Borrar of a graph type "<tipo_grafico>"
  Then I should not see that graph/chart in the list.
Examples: 
    | tipo_grafico     |
    | Barra Horizontal |
    | Contador |
    | Circular |
