Feature: View all the graphs generated on reports - events page
  As administrador
  I want to see all the graphs created
  so I can see the results in a graphic way that the report of events has thrown in "Reportes - Eventos" page 

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Eventos page

@maximize
Scenario: See all previously created graphs     
  Given I am on the Reportes - Eventos page
  When I count all the old charts created  
  Then I should have more than 1 charts.

@maximize
Scenario: See more details from a graph 
  Given I am on the Reportes - Eventos page
  When I click the eye icon from the graph "<nombre_grafica>"
  Then I should see the graph "<nombre_grafica>" on the entire page.
Examples:
   | nombre_grafica          |
   | Grafico Lineas          |
   | Grafico Circular        |
   | Grafico Area Agrupada   |
   | Grafico Barra Apilada   |
   | Grafico Barra Agrupada  |
   | Grafico Barra Simple    |
