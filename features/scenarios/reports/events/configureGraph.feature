Feature: Configure a graph of events
  As a user of DigitalAll Expirence
  I want to configure a graph of event
  so I see a information on a graph with my settings

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Eventos page

@maximize
Scenario: Configure a graph
  Given I am on the Reportes - Eventos page
  When I select a graph with name "<nombre_grafico>" and type "<grafico>" 
  Then I should see the message "Configuracion Actualizada con éxito!" 
    And see the new graph with name "<nombre_grafico>".
Examples:
    |  grafico            | nombre_grafico          |
    |  Lineas             | Grafico Lineas          |
    |  Circular           | Grafico Circular        |
    |  Area Agrupada      | Grafico Area Agrupada   |
    |  Barra Apilada      | Grafico Barra Apilada   |
    |  Barra Agrupada     |	Grafico Barra Agrupada  |
    |  Barra Simple       | Grafico Barra Simple    |
  