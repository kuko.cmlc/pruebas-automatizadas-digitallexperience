Feature: Delete a graph generated on reports - events page
  As a owner of a business
  I want to delete a graph created previously
  so I can remove the graphs that are unnecessary for the reports

Background: User is Logged In and navigate to Eventos page
  Given I navigate to the login page
  And I submit username and password 
  And I Navigate to Reportes - Eventos page

@maximize
Scenario: Delete a specific graph 
  Given I have a graph "<nombre_grafica>" created before on the Reportes - Eventos page
  When I click the trash icon from the graph "<nombre_grafica>"
  Then I should see all the graphs except the graph that was removed.
Examples:
   | nombre_grafica          |
   | Grafico Lineas          |
   | Grafico Circular        |
   | Grafico Area Agrupada   |
   | Grafico Barra Apilada   |
   | Grafico Barra Agrupada  |
   | Grafico Barra Simple    |
