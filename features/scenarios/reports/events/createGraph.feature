Feature: Create a new graph to configure on Eventos Page
  As a Administrator
  I want to create a new graph from a campaign results
  so I can see the graph for my reports.

Background: User is Logged In and navigate to Eventos page
 Given I navigate to the login page
 And I submit username and password 
 And I Navigate to Reportes - Eventos page
 And Press the bottom Agregar Grafica

Scenario: Create a new graph
  Given I am on the form for create a new chart
  When I create a new "<grafico>" type graph with name "<nombre_grafico>" 
  Then I should see the new graph with name "<nombre_grafico>".
  
Examples:
    |  grafico            | nombre_grafico          |
    |  Barra Simple       | Grafico Barra Simple    |
    |  Barra Agrupada     |	Grafico Barra Agrupada  |
    |  Barra Apilada      | Grafico Barra Apilada   |
    |  Area Agrupada      | Grafico Area Agrupada   |
    |  Circular           | Grafico Circular        |
    |  Lineas             | Grafico Lineas          |