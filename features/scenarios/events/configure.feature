Feature: Edit an event
  As a user of DigitalAll Expirence
  I want to edit an event
  so I update de information of the event

Background: User is Logged In and navigate to Eventos page
  Given I navigate to the login page
  And I submit username and password 
  And I am on home page of event 

@editEmptyData
Scenario: Configure an event with empty data 
  Given I select an event
  And I enter empty data
  And I press the Editar button of event
  Then I should see the message after edit "El campo es obligatorio" 

@validData
Scenario: Configure an event with valid data 
  Given I select an event
  And I enter the required fields of the event
  And I press the Editar button of event
  Then I should see the message after edit "Evento editado con éxito" 