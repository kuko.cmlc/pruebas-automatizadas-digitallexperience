require 'capybara/dsl'

class ViewGraph
  include Capybara::DSL
  Capybara.run_server = false

  def validateIAmOnEventosPage
    title = find(:css , '.col-lg-12 > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1)');
    if title.text != "Reportes de eventos"
      raise "There an error with title page, the title was" + title.text
    end 
    page.has_css?('.navbar')
    page.has_css?('div.row:nth-child(3)')
  end

  def clickEyeIcon(nameGraph)
    lastGraph = all('svg').size 
    case nameGraph
    when "Grafico Lineas"
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    when "Grafico Circular"
      lastGraph = lastGraph - 1
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    when "Grafico Area Agrupada"
      lastGraph = lastGraph - 2
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    when "Grafico Barra Apilada"
      lastGraph = lastGraph - 3
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    when "Grafico Barra Agrupada"
      lastGraph = lastGraph - 4
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    when "Grafico Barra Simple"
      lastGraph = lastGraph - 5 
      buttonEyePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)"
      find(:css, buttonEyePath).click
    else
      raise "Can't see more details of the graph"
    end
  end

  def validateGraphDisplayed(nameGraph)
    lastGraph = all('svg').size 
    case nameGraph
    when "Grafico Lineas"
      validateSeeMoreGraphLineas
    when "Grafico Circular"
      validateSeeMoreGraphCircular
    when "Grafico Area Agrupada"
      validateSeeMoreGraphAreaAgrupada
    when "Grafico Barra Apilada"
      validateSeeMoreGraphBarraApilada
    when "Grafico Barra Agrupada"
      validateSeeMoreGraphBarraAgrupada
    when "Grafico Barra Simple"
      validateSeeMoreGraphBarraSimple
    else
      raise "Can't see more details of the graph"
    end
  end

  
  def validateSeeMoreGraphLineas
    lastValueY = find(:css, "body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > svg > g > g:nth-child(2) > g > g:nth-child(1) > g:nth-child(1) > g:nth-child(7) > text").text
    firstValueY = find(:css, "body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > svg > g > g:nth-child(2) > g > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > text").text
    firstLabel = find(:css, "body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > ngx-charts-legend > div > div > ul > li:nth-child(1) > ngx-charts-legend-entry > span > span.legend-label-text").text
    secondLabel = find(:css,"body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > ngx-charts-legend > div > div > ul > li:nth-child(2) > ngx-charts-legend-entry > span > span.legend-label-text").text
    thirdLabel = find(:css, "body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > ngx-charts-legend > div > div > ul > li:nth-child(3) > ngx-charts-legend-entry > span > span.legend-label-text").text
    lastLabel = find(:css, "body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > line-chart > ngx-charts-line-chart > ngx-charts-chart > div > ngx-charts-legend > div > div > ul > li:nth-child(4) > ngx-charts-legend-entry > span > span.legend-label-text").text
    if (lastValueY == '30'&& firstValueY == '0' && firstLabel == '2020-07-29 23:00' && secondLabel == '2020-07-30 00:00' && thirdLabel == '2020-07-30 09:00' && lastLabel == '2020-07-30 10:00')
      find(:css, ".close").click
    end
  end

  def validateSeeMoreGraphCircular
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > pie-chart > ngx-charts-pie-chart > ngx-charts-chart > div > svg")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-header")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > pie-chart > ngx-charts-pie-chart > ngx-charts-chart > div > svg > g > g > g:nth-child(2) > g:nth-child(2) > g > path")
  end

  def validateSeeMoreGraphAreaAgrupada
    raise "There is an error with this graph, Bug not fixed"
  end

  def validateSeeMoreGraphBarraApilada
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(4) > g > g:nth-child(1) > path")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(4) > g > g:nth-child(2) > path")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(4) > g > g:nth-child(3) > path")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(4) > g > g:nth-child(4) > path")
    
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(3) > g > g:nth-child(1) > path")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > stacked-bar-chart > ngx-charts-bar-vertical-stacked > ngx-charts-chart > div > svg > g > g:nth-child(3) > g > g:nth-child(2) > path")
  end

  def validateSeeMoreGraphBarraAgrupada  
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > grouped-bar-chart > ngx-charts-bar-vertical-2d > ngx-charts-chart > div > svg")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > grouped-bar-chart > ngx-charts-bar-vertical-2d > ngx-charts-chart > div > ngx-charts-legend > div > div > ul") 
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-header")
  end

  def validateSeeMoreGraphBarraSimple
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > bar-chart > ngx-charts-bar-vertical > ngx-charts-chart > div > svg")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-body > div > div > div > div > bar-chart > ngx-charts-bar-vertical > ngx-charts-chart > div > ngx-charts-legend > div > div > ul")
    page.has_css?("body > modal-container > div > div > app-full-view > div > div.modal-header")
  end
end