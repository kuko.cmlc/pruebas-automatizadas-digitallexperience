require 'capybara/dsl'
class DeleteGraph
  include Capybara::DSL
  Capybara.run_server = false
  beforeDelete = 0
  afterDelete = 0
  def deleteGraphCampaingPage(typeGraph)
    sleep 0.5
    lastGraph = all('chart').size 
    @beforeDelete = all('chart').size 
    case typeGraph
    when 'Barra Horizontal'
      title = find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-10.header > span")
      if title.text == 'Grafica Barra Horizontal'
        find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(3) > i").click
      end   
    when 'Contador' 
      title = find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-10.header > span")
      if title.text == 'Grafica Contador'
        find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(3) > i").click
      end   
    when 'Circular'
      title = find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-10.header > span")
      if title.text == 'Nuevo Chart'
        find(:css,"body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastGraph}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(3) > i").click
      end   
    else
      raise "Graph was not deleted, there is a problem with the names or components"
    end
    find(:css , 'body > modal-container > div > div > div.modal-footer > button.btn.btn-outline-primary.btn-sm.button-create').click
  end


  def validateGraphDeleted
    sleep 0.5
    @afterDelete  = all('chart').size 
    if (@beforeDelete <= @afterDelete)
      raise "Graph was not deleted"
    end
  end

  def validateGraphExist(graphName)
    @beforeDelete = all('svg').size
    lastGraph = all('svg').size
    titleGraph = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1)"
    if (find(:css, titleGraph).text != graphName)
      raise "Graph was not found"
    end
  end

  def deleteGraphFromEventPage(graphName)
    lastGraph = all('svg').size
    buttonDeletePath = "div.col-6:nth-child(#{lastGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > i:nth-child(1)"
    find(:css, buttonDeletePath).click
    confirmButton = "button.btn:nth-child(2)"
    find(:css, confirmButton).click
    @afterDelete = all('svg').size - 1
  end

  def validateGraphDeletedFromEventPage
    if (@beforeDelete <= @afterDelete)
      raise "Graph was not deleted"
    end
  end
end    