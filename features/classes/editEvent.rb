require 'capybara/dsl'

class EditEvent 
  include Capybara::DSL
  Capybara.run_server = false

  def validateIamOnPageEvents
    title=find(:css,"body > app-root > div > div > app-events > div > div:nth-child(3) > div.col-8.text-right.vertical-align-center > span")
    if title.text != "Filtrar por Fecha:"
      raise "There an error with title page, the title was" + title.text
    end 
  end

  def selectEvent
    find(:css,"body > app-root > div > div > app-events > div > div.row.events-dae > div > div:nth-child(1) > div > event-item > div.pt-1.pb-1.pr-0.row.event-table-item > div.col-2.pr-0.event-table-icon > div > i.fa.fa-pencil-square-o").click
  end
  def setCommonTexts
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(6) > div:nth-child(1) > select > option").click
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(6) > div:nth-child(2) > select > option").click
  end
  def emptyTheFields
    sleep 1
    find(:css,"div.row:nth-child(4) > div:nth-child(1) > label:nth-child(2) > input:nth-child(1)").click
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(5) > div:nth-child(1) > select > option:nth-child(1)").click
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(5) > div:nth-child(2) > input").set("")
    setCommonTexts()
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(7) > textarea").set("")
  end

  def pressButton
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-footer > button.btn.btn-outline-primary.btn-sm.button-edit").click
  end

  def validData
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(5) > div:nth-child(1) > select > option:nth-child(3)").click
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(5) > div:nth-child(2) > input").set("1")
    setCommonTexts()
    find(:css,"body > app-root > div > div > app-edit-event > div > form > div.modal-body > div:nth-child(7) > textarea").set("Este es un comentario de prueba")
  end

end