require 'capybara/dsl'

class Navigation
  include Capybara::DSL
  Capybara.run_server = false
  @@xpath_Reportes_page = 'li.nav-item:nth-child(5) > div:nth-child(1) > a:nth-child(1)'
  def navigateToReportesEventosPage
    xpath_Eventos_page = 'div.show:nth-child(2) > a:nth-child(2)'
    find(:css, @@xpath_Reportes_page).click
    find(:css, xpath_Eventos_page).click
  end

  def navigateToReportesCampañaPage
    xpath_Campaña_page = 'div.show:nth-child(2) > a:nth-child(1)'
    find(:css, @@xpath_Reportes_page).click
    find(:css, xpath_Campaña_page).click
  end
end