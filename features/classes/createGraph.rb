require 'capybara/dsl'
class CreateGraph
  include Capybara::DSL
  Capybara.run_server = false
  def validateModalDisplayed
    title = find(:css, '.modal-title')
    page.has_css?('input.form-control')
    page.has_css?('select.form-control')
    page.has_css?('button.btn:nth-child(2)')
    page.has_css?('button.btn-sm:nth-child(1)')
    if title.text != 'Agregar Gráfica'
        raise "There is an error with the title."	
    end
  end

  def setName(nameGraph)
    fill_in('title', :with => nameGraph)
  end

  def selectType(typeGraph)
    case typeGraph
      when 'Barra Simple'
        find(:css, "select.form-control > option:nth-child(1)").click  
      when 'Barra Agrupada'
        find(:css, "select.form-control > option:nth-child(2)").click  
      when 'Barra Apilada'
        find(:css, "select.form-control > option:nth-child(3)").click       
      when 'Area Agrupada'
        find(:css, "select.form-control > option:nth-child(4)").click  
      when 'Circular'
        find(:css, "select.form-control > option:nth-child(5)").click  
      when 'Lineas'
        find(:css, "select.form-control > option:nth-child(6)").click  
      else
        raise "There is an error selecting type graph."      
      end  
  end

  def createGraph(name,type)
    setName(name)
    selectType(type)
    css_path = 'button.btn:nth-child(2)'
    find(:css, css_path).click
  end

  def checkGraphCreated(titleGraph)
    sleep 1
    positionNewGraph = all('svg').size 
    cssTitlePath = find(:css , "div.col-6:nth-child(#{positionNewGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1)")     
    # Validate chart created
    page.has_css?("div.col-6:nth-child(#{positionNewGraph}) > chart:nth-child(1)")
    # Validate buttons "Edit,Delete,View more" created
    page.has_css?("div.col-6:nth-child(#{positionNewGraph}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)")
    if cssTitlePath.text != titleGraph
      raise "Error, graph not created " + titleGraph	
    end
  end 
end    


