require 'capybara/dsl'
class ConfigureGraph
  include Capybara::DSL
  Capybara.run_server = false
  
  pathCss_InitialDate = 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(2) > div:nth-child(1) > div > input'
  pathCss_FinalDate = 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(2) > div:nth-child(3) > div > input'
  pathCss_TPSRL = 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(4) > div > div > select > option:nth-child(10)'
  pathCss_Button = 'button.btn:nth-child(2)'
  
  def selectGraph (nameGraph,typeGraph)
    sleep 0.5
    lastChart = all('chart').size 
    case typeGraph
    when 'Contador'
      find(:css, "div.col-6:nth-child(#{lastChart}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > i:nth-child(1)").click
    when 'Barra Horizontal'
      lastChart = lastChart + 1
      createGraph(nameGraph)
      find(:css, "div.col-6:nth-child(#{lastChart}) > chart:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > i:nth-child(1)").click
    else
      raise "Type graph does not exist."
    end
  end

  def setDate (initialDate,finalDate)
    cssPath_initialDate = 'div.col-3:nth-child(1) > div:nth-child(1) > input:nth-child(2)'
    cssPath_finalDate ='div.col-3:nth-child(3) > div:nth-child(1) > input:nth-child(2)'
    find(:css, cssPath_initialDate).set(initialDate)
    find(:css, cssPath_finalDate).set(finalDate)
  end

  def setSRLcompain()
    find(:css , 'option.ng-star-inserted:nth-child(10)').click
  end

  def submitConfiguration 
    cssPath_button = 'button.btn:nth-child(2)'
    find(:css, cssPath_button).click
  end

  def setCommonTexts()
    #Regional="Cochabamba"
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(7) > div:nth-child(1) > div > select > option:nth-child(2)' ).click
    #Agencia="Central"
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(7) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Servicios="Plataforma"
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(8) > div:nth-child(1) > div > select > option:nth-child(3)').click
    #Puntos de servicio=Ninguno
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(8) > div:nth-child(2) > div > select > option:nth-child(1)').click
  end
  
  def createGraph(nameGraph)
    cssPath_button_create_graph = '.btn'
    find(:css , cssPath_button_create_graph).click
    fill_in('title', :with => nameGraph)
    find(:css, "select.ng-valid > option:nth-child(5)").click  
    find(:css, "option.ng-star-inserted:nth-child(3)").click  
    find(:css, "button.btn:nth-child(2)").click
  end

  def startConfigureGraph (nameGraph,typeGraph)
    lastChart = all('svg').size 
    case typeGraph
    when 'Contador'
      setValidDataGraphContador(nameGraph)
    when 'Barra Horizontal'
      setValidDataGraphBarraHorizontal(nameGraph)  
    when 'Lineas'
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphLineas("29-07-2020","15-08-2020")
    when 'Circular'
      lastChart = lastChart -1
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphCircular("29-07-2020","15-08-2020")
    when 'Area Agrupada'
      lastChart = lastChart -2
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphAreaAgrupada("29-07-2020","15-08-2020")
    when 'Barra Apilada'
      lastChart = lastChart -3
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphBarraApilada("15-07-2020","15-08-2020")
    when 'Barra Agrupada'
      lastChart = lastChart -4
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphBarraAgrupada("29-07-2020","15-08-2020")
    when 'Barra Simple'
      lastChart = lastChart -5
      find(:css , "body > app-root > div > div > app-event-reports > div > div:nth-child(3) > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-header > div:nth-child(1) > div.col-2.pr-0 > div > div:nth-child(2) > i").click
      setValidDataGraphBarraSimple("29-07-2020","15-08-2020")
    else
      raise "Type graph does not exist."
    end
  end

  def setValidDataGraphContador(nameGraph)
    fill_in('title', :with => nameGraph)
    cssPath_typeGraph = '.modal-body > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > select:nth-child(2) > option:nth-child(9)'
    find(:css, cssPath_typeGraph).click
    setDate("03-12-2018","03-12-2020")
    submitConfiguration()
  end

  def setValidDataGraphBarraHorizontal(nameGraph)
    fill_in('title', :with => nameGraph)
    cssPath_typeGraph = '.modal-body > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > select:nth-child(2) > option:nth-child(2)'
    find(:css, cssPath_typeGraph).click
    setDate("03-12-2018","03-12-2020")
    find(:css,'.modal-body > div:nth-child(5) > div:nth-child(1) > div:nth-child(1) > select:nth-child(2) > option:nth-child(2)').click
    find(:css,'.modal-body > div:nth-child(5) > div:nth-child(2) > div:nth-child(1) > select:nth-child(2) > option:nth-child(2)').click
    find(:css,'div.ng-star-inserted:nth-child(6) > div:nth-child(1) > div:nth-child(1) > select:nth-child(2) > option:nth-child(2)').click
    find(:css, 'div.ng-star-inserted:nth-child(7) > div:nth-child(1) > div:nth-child(1) > select:nth-child(2) > option:nth-child(4)')
    find(:css,'select.ng-star-inserted > option:nth-child(2)').click
    find(:css,'div.ng-star-inserted:nth-child(8) > div:nth-child(1) > div:nth-child(1) > select:nth-child(2) > option:nth-child(4)').click
    submitConfiguration()
  end

  def validateGraphConfigurated (typeGraph) 
    lastChart = all('chart').size 
    case typeGraph
    when 'Contador'
      page.has_css?("body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-body.row > counter > div:nth-child(1) > div")     
   when 'Barra Horizontal'
      page.has_css?("body > app-root > div > div > app-reports > div > ng-sidebar-container > div > div > div:nth-child(2) > div > div:nth-child(#{lastChart}) > chart > div > div > div > div.card-body.row > horizontal-bar > ngx-charts-bar-horizontal > ngx-charts-chart > div > ngx-charts-legend > div > div > ul > li > ngx-charts-legend-entry > span > span.legend-label-text")
    else
      raise "Graph not configurated."
    end 
  end


  def setValidDataGraphLineas(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Eje x=Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Eje y=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Grupo=Tiempo
    find(:css, 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Intervalo=Hora
    find(:css, 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(11) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end

  def setValidDataGraphCircular(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Grupo=Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Valor=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option.ng-star-inserted').click
    #Intervalo=Hora
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end

  def setValidDataGraphAreaAgrupada(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Grupo=Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Eje y=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Eje x= Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Intervalo=Hora
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(11) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end

  def setValidDataGraphBarraApilada(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Grupo=Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Eje y=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Eje x= Estado
    find(:css, 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div:nth-child(1) > div > select > option:nth-child(4)').click
    #Intervalo=Hora
    find(:css, 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(11) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end

  def setValidDataGraphBarraAgrupada(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Grupo=Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Eje y=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Eje x= Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Intervalo=Hora
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(11) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end

  def setValidDataGraphBarraSimple(initialDate,finalDate) 
    setDate(initialDate,finalDate)
    setSRLcompain()
    setCommonTexts()
    #Eje x= Tiempo
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(1) > div > select > option:nth-child(2)').click
    #Eje y=Cantidad
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(9) > div:nth-child(2) > div > select > option:nth-child(2)').click
    #Intervalo=Hora
    find(:css , 'body > modal-container > div > div > app-event-settings > div.modal-body > div:nth-child(10) > div > div > select > option:nth-child(2)').click
    submitConfiguration
  end
end